﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace LimeMoonBot.Services
{
    public class CommandHandlingService
    {
        private readonly CommandService _commands;
        private readonly DiscordShardedClient _client;
        private readonly IServiceProvider _services;

        public CommandHandlingService(IServiceProvider services)
        {
            _commands = services.GetRequiredService<CommandService>();
            _client = services.GetRequiredService<DiscordShardedClient>();
            _services = services;

            _commands.CommandExecuted += CommandExcutedAsync;
            _commands.Log += LogAsync;
            _client.MessageReceived += MessageReceivedAsync;
        }

        public async Task InitializeAsync()
        {
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), _services);
        }

        public async Task MessageReceivedAsync(SocketMessage msg)
        {
            if (!(msg is SocketUserMessage message)) return;
            if (message.Source != MessageSource.User) return;

            var argPos = 0;
            //if (!message.HasMentionPrefix(_client.CurrentUser, ref argPos)) return;

            // Command prefix
            if (!message.HasCharPrefix('!', ref argPos)) return;

            var context = new ShardedCommandContext(_client, message);
            await _commands.ExecuteAsync(context, argPos, _services); 
        }

        private Task LogAsync(LogMessage arg)
        {
            // TODO : LOG
            MainWindow.LogText = arg.ToString();
            return Task.CompletedTask;
        }

        public async Task CommandExcutedAsync(Optional<CommandInfo> cmd, ICommandContext context, IResult result)
        {
            if (!cmd.IsSpecified) return;

            if (result.IsSuccess) return;

            await context.Channel.SendMessageAsync($"error : {result.ToString()}");
        }
    }
}