﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using LimeMoonBot.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace LimeMoonBot
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
        static public string LogText = "";

        #region Discord dotnet

        private DiscordShardedClient _client;
        private IServiceProvider _services;

        private async Task MessageReceivedAsync(SocketMessage msg)
        {
            if (msg.Author.Id == _client.CurrentUser.Id)
                return;

            if (msg.Content == "!ping")
                await msg.Channel.SendMessageAsync("pong!");
        }

        private Task ReadyAsync(DiscordSocketClient shard)
        {
            MessageBox.Show($"Shard Number {shard.ShardId} is Ready");
            
            return Task.CompletedTask;
        }
        private Task LoggedInAsync() { MessageBox.Show("LoggedIn"); return Task.CompletedTask; }
        private Task LoggedOutAsync() { MessageBox.Show("LoggedOut"); return Task.CompletedTask; }

        private Task LogAsync(LogMessage msg)
        {
            // TODO : Log
            LogText += msg.ToString() + "\n";
            //tblck_log.Text = LogText;
            this.Dispatcher.BeginInvoke((ThreadStart)delegate()
            {
                tblck_log.Text = LogText;
            });
            return Task.CompletedTask;
        }

        public async Task InitAsync(string token)
        {
            // Init
            var config = new DiscordSocketConfig
            {
                TotalShards = 2
            };

            _client = new DiscordShardedClient(config);
            _services = ConfigureServices();

            // Log
            _client.Log += LogAsync;
            _services.GetRequiredService<CommandService>().Log += LogAsync;

            // Ready
            _client.ShardReady += ReadyAsync;

            // LogInOut
            _client.LoggedIn += LoggedInAsync;
            _client.LoggedOut += LoggedOutAsync;

            _client.MessageReceived += MessageReceivedAsync;

            // Add Token . Do not hard-coded
            await _client.LoginAsync(TokenType.Bot, token);
            await _client.StartAsync();

            await _services.GetRequiredService<CommandHandlingService>().InitializeAsync();

            await Task.Delay(-1);
        }

        //public void Init(string token)
        //{

        //    // Init
        //    _services = ConfigureServices();
        //    _client = _services.GetRequiredService<DiscordSocketClient>();

        //    // Log
        //    _client.Log += LogAsync;
        //    _services.GetRequiredService<CommandService>().Log += LogAsync;

        //    // Ready
        //    _client.Ready += ReadyAsync;

        //    // LogInOut
        //    _client.LoggedIn += LoggedInAsync;
        //    _client.LoggedOut += LoggedOutAsync;

        //    // Add Token . Do not hard-coded
        //    _client.LoginAsync(TokenType.Bot, token);
        //    _client.StartAsync();

        //    _services.GetRequiredService<CommandHandlingService>().InitializeAsync();
        //}

        private IServiceProvider ConfigureServices()
        {
            return new ServiceCollection()
                .AddSingleton(_client)
                .AddSingleton<CommandService>()
                .AddSingleton<CommandHandlingService>()
                .AddSingleton<HttpClient>()
                .AddSingleton<PictureService>()
                .BuildServiceProvider();
        }
        #endregion


        public MainWindow()
        {
            InitializeComponent();

            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (tblck_log.Text == LogText)
                return;

            tblck_log.Text = LogText;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        //=> InitAsync(tbox_token.Text).GetAwaiter().GetResult();
        {
            InitAsync(tbox_token.Text);
            //Init(tbox_token.Text);
        }

        private void btn_end_Click(object sender, RoutedEventArgs e)
        {
            _client.LogoutAsync();
            _client.StopAsync();
        }
    }
}
